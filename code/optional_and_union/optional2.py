from typing import List,Dict, Union

database: Dict[int,Union[str,int,float]] = {
    1 : 'a' , 2 : 222222 , 3 : 'c' , 5 : 12.3 , 6: 'f'
}

def get_by_id(id:int)-> Union[str,int,float,None]:
    if id in database:
        return database[id]
    return None

def print_it_n_times(s:Union[str,int,float],n:int) -> None:
    print(s*n)

value = get_by_id(4)

reveal_type(value)
# Revealed type is 'Union[builtins.str, builtins.int, builtins.float, None]'

if value is not None:
    reveal_type(value)
    # Revealed type is 'Union[builtins.str, builtins.float]'

    print_it_n_times(value,4)
