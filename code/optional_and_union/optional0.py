database = {
    1 : 'a' , 2 : 222222 , 3 : 'c' , 5 : 12.3 , 6: 'f'
}

def get_by_id(id):
    if id in database:
        return database[id]
    return None

def print_it_n_times(s,n):
    print(s*n)

value = get_by_id(4)

print_it_n_times(value,4)

