import string
from typing import List

characters = string.ascii_letters + string.digits
count = len(characters)


def encode(s, n):
    # type: (str, int) -> str
    encoded = [_encode_char(char, n) for char in s]  # type: List[str]
    return ''.join(encoded)


def decode(s, n):
    # type: (str, int) -> str
    decoded = [_decode_char(char, n) for char in s]  # type: List[str]
    return ''.join(decoded)


def _encode_char(char, n):
    # type: (str, int) -> str
    if char not in characters:
        return char
    original_index = characters.index(char)  # type: int
    new_index = original_index + n           # type: int
    fixed_index = new_index % count          # type: int
    return characters[fixed_index]


def _decode_char(char, n):
    # type: (str, int) -> str
    return _encode_char(char, -n)


def test_encode_char():
    # type: () -> None
    assert _encode_char('a', 1) == 'b'
    assert _encode_char('z', 1) == 'A'
    assert _encode_char('Z', 1) == '0'
    assert _encode_char('9', 1) == 'a'

    assert _encode_char('a', len(characters) + 1) == 'b'
    assert _encode_char('z', len(characters) + 1) == 'A'
    assert _encode_char('Z', len(characters) + 1) == '0'
    assert _encode_char('9', len(characters) + 1) == 'a'


def test_decode_char():
    # type: () -> None
    assert _decode_char('b', 1) == 'a'
    assert _decode_char('A', 1) == 'z'
    assert _decode_char('0', 1) == 'Z'
    assert _decode_char('a', 1) == '9'

    assert _decode_char('b', len(characters) + 1) == 'a'
    assert _decode_char('A', len(characters) + 1) == 'z'
    assert _decode_char('0', len(characters) + 1) == 'Z'
    assert _decode_char('a', len(characters) + 1) == '9'


def test_string_encode_decode():
    # type: () -> None
    assert encode('azZ9', 1) == 'bA0a'
    assert decode('bA0a', 1) == 'azZ9'
    s = "Th3 qu1ck br0wn f0x jump5 over the lazy dog, and away he goes!" # type: str
    assert decode(encode(s, 1), 1) == s
    assert decode(encode(s, 10), 10) == s
    assert decode(encode(s, 100), 100) == s
