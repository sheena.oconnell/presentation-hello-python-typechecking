import copy
import random


def default_key(a, b) -> bool:
    return a > b


def bubble_sort(to_sort, key=None):
    l = copy.copy(to_sort)
    if key == None:
        key = default_key

    assert key is not None

    unsorted = True
    last_index = len(l)-1

    while unsorted:
        unsorted = False
        for n in range(last_index):
            first = l[n]
            second = l[n+1]
            if key(first, second):
                l[n]   = second
                l[n+1] = first
                if n+1 != last_index:
                    unsorted = True
    return l


def _reverse_key(a,b):
    return a < b

def test_sort():
    l = [0,1,2,3,4,5,6,7,8,9]

    for _ in range(5):
        shuffled = copy.copy(l)
        random.shuffle(shuffled)
        assert shuffled != l
        shuffled = bubble_sort(shuffled)
        assert shuffled == l

    reversed = l[::-1]

    for _ in range(5):
        shuffled = copy.copy(l)
        random.shuffle(shuffled)
        assert shuffled != l
        shuffled = bubble_sort(shuffled,key=_reverse_key)
        assert shuffled == reversed
