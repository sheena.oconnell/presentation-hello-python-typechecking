from typing import TypeVar, Optional,Callable,List,overload
from typing_extensions import Protocol
import copy
import random


# https://mypy.readthedocs.io/en/latest/protocols.html
# https://mypy.readthedocs.io/en/latest/generics.html
# https://mypy.readthedocs.io/en/latest/more_types.html#function-overloading


class SupportsGreaterThan(Protocol):
    def __gt__(self,other)-> bool:
        ...


T = TypeVar('T')
TComparible = TypeVar('TComparible', bound=SupportsGreaterThan)

def default_key(a:TComparible, b:TComparible) -> bool:
    return a > b


@overload
def bubble_sort(to_sort: List[T], key: Callable[[T,T],bool]) -> List[T]:
    ...

@overload
def bubble_sort(to_sort: List[TComparible], key: None )-> List[TComparible]:
    ...


def bubble_sort(to_sort: List[T], key=None) -> List[T]:
    l: List[T] = copy.copy(to_sort)
    if key == None:
        key = default_key

    assert key is not None

    unsorted: bool = True
    last_index: int = len(l)-1

    while unsorted:
        unsorted = False
        for n in range(last_index):
            first: T = l[n]
            second: T = l[n+1]
            if key(first, second):
                l[n]   = second
                l[n+1] = first
                if n+1 != last_index:
                    unsorted = True
    return l

def _reverse_key(a,b):
    return a < b


def test_sort():
    l = [0,1,2,3,4,5,6,7,8,9]

    for _ in range(5):
        shuffled = copy.copy(l)
        random.shuffle(shuffled)
        assert shuffled != l
        shuffled = bubble_sort(shuffled)
        assert shuffled == l

    reversed = l[::-1]

    for _ in range(5):
        shuffled = copy.copy(l)
        random.shuffle(shuffled)
        assert shuffled != l
        shuffled = bubble_sort(shuffled,key=_reverse_key)
        assert shuffled == reversed
