from typing import TypeVar, Sequence, Optional

T = TypeVar('T')      # Declare type variable

def first(seq: Sequence[T]) -> T:
   return seq[0]

def last(seq: Sequence[T]) -> T:
   return seq[-1]


a = first([1,2,3])
reveal_type(a) # int

b = last([1,2,3])
reveal_type(b) # int

c = first(['a','b','c'])
reveal_type(c) # str

d = last(['a','b','c'])
reveal_type(d) # str
