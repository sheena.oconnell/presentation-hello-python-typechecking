import random
from typing import TypeVar, List, Iterator
from typing_extensions import Protocol

class SupportsIsValid(Protocol):
    def is_valid(self) -> bool:
        ...

class Foo:
    def is_valid(self) -> bool:
        return random.choice([True,False])

class Bar:
    def is_valid(self) -> bool:
        return random.choice([True,False])

def valid_instances(l: List[SupportsIsValid]) -> List[SupportsIsValid]:
    return [x for x in l if x.is_valid()]

l: List[SupportsIsValid] = []
l.extend([Foo() for _ in range(3)])
l.extend([Bar() for _ in range(3)])

valid = valid_instances(l)
