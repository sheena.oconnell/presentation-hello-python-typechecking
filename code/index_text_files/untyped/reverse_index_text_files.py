import os
import re
import json
import logging
logging.basicConfig(level=logging.INFO)


class WordEntry:
    def __init__(self):
        self.files = {}
        self.total_count = 0

    def add_file_reference(self, file_name, line):
        self.total_count += 1
        if file_name not in self.files:
            self.files[file_name] = {}
        file_info = self.files[file_name]
        if line not in file_info:
            file_info[line] = 0
        file_info[line] += 1


def register_word(index, word, file_name, line):
    if not word.isalpha():
        return
    word = word.lower()
    ignore = ['a', 'the', 'of', 'and', 'to', 'is', 'that', 'in', 'by', 'for',
              'it', 'not', 'or', 'but', 'as', 'be', 'which']
    if word in ignore:
        return
    if word not in index:
        index[word] = WordEntry()
    entry = index[word]
    entry.add_file_reference(file_name, line)


def index_file(index, f, file_name):
    for n, line in enumerate(f):
        for word in re.findall(r'\w+', line):
            register_word(index, word, file_name, n)


def index_all_files(index_dir):
    index = {}
    for name in os.listdir(index_dir):
        logging.info(f"indexing {name}")
        full_path = os.path.join(index_dir, name)
        if os.path.isdir(full_path):
            continue
        with open(full_path, 'r') as f:
            index_file(index, f, name)
    return index


def save_index_to_file(index, file_name):
    index_json = {
        word: index[word].__dict__
        for word in index
    }
    with open(file_name, 'w') as f:
        json.dump(index_json, f, sort_keys=True, indent=2)


def most_common_words(index, count):
    totals = sorted(((index[word].total_count, word) for word in index),
                    reverse=True)
    return totals[:count]


if __name__ == '__main__':
    index_dir = 'downloads'
    index = index_all_files(index_dir)
    top_words = most_common_words(index, 10)
    pretty_words = {json.dumps(top_words, indent=2, sort_keys=True)}
    save_index_to_file(index, 'index.json')
