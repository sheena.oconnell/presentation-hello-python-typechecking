class Amoeba:
    min_divide_weight  = 100
    weight_gain_per_eat = 10

    def __init__(self,weight=50):
        self.weight = weight

    def divide(self):
        assert self.weight >= self.min_divide_weight, "too small"
        child_weight = self.weight//2 + self.weight%2
        return Amoeba(child_weight)

    def eat(self):
        self.weight += self.weight_gain_per_eat

class RedAmoeba:
    """ Breed a bit faster than the average amoeba """
    min_divide_weight = 90

class GreenAmoeba:
    """ Use food more efficiently """
    weight_gain_per_eat = 12



reds = [RedAmoeba() for i in range(10)]
greens = [GreenAmoeba() for i in range(10)]



