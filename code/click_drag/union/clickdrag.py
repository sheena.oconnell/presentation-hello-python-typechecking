from typing import Union, Optional

class Event:
    def __init__(self,*args):
        pass
class ClickEvent(Event): pass
class DragEvent(Event): pass

def mouse_event(x1: int,
                y1: int,
                x2: Optional[int] = None,
                y2: Optional[int] = None) -> Union[ClickEvent, DragEvent]:
    if x2 is None and y2 is None:
        return ClickEvent(x1, y1)
    elif x2 is not None and y2 is not None:
        return DragEvent(x1, y1, x2, y2)
    else:
        raise TypeError("Bad arguments")

reveal_type(mouse_event(1,2))     # Union[ClickEvent, DragEvent]
reveal_type(mouse_event(1,2,3))   # Union[ClickEvent, DragEvent]
reveal_type(mouse_event(1,2,3,4)) # Union[ClickEvent, DragEvent]
