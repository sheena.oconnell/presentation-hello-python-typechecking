from typing import Union, Optional, overload

class Event:
    def __init__(self,*args):
        pass
class ClickEvent(Event): pass
class DragEvent(Event): pass

@overload
def mouse_event(x1: int, y1: int) -> ClickEvent:
    ...

@overload
def mouse_event(x1: int, y1: int, x2: int, y2: int) -> DragEvent:
    ...

def mouse_event(x1: int,
                y1: int,
                x2: Optional[int] = None,
                y2: Optional[int] = None) -> Union[ClickEvent, DragEvent]:
    if x2 is None and y2 is None:
        return ClickEvent(x1, y1)
    elif x2 is not None and y2 is not None:
        return DragEvent(x1, y1, x2, y2)
    else:
        raise TypeError("Bad arguments")


reveal_type(mouse_event(1,2))     # ClickEvent
reveal_type(mouse_event(1,2,3))   # No overload varient match
reveal_type(mouse_event(1,2,3,4)) # DragEvent
