from typing import ClassVar

class Spam:
    egg: ClassVar[int] = 5
    cheese: str = "cheddar"

inst = Spam()
inst2 = Spam()

print(inst.egg)
print(inst.cheese)

Spam.cheese = "gouda"
inst.cheese = "parmasan"

print(inst.cheese)
print(inst2.cheese)
print(Spam.cheese)


Spam.egg = "poached"
inst.egg = 12
Spam.cheese = 12


