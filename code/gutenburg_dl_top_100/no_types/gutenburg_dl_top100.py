import requests
from lxml import html  # type: ignore
import re
import os
import logging
logging.basicConfig(level=logging.INFO)


def setup_dir(save_dir):
    logging.info('setting up directory')
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    logging.info('...done')


def get_book_link_info():
    logging.info('fetching book list')
    url = "https://www.gutenberg.org/browse/scores/top"
    page = requests.get(url)
    assert page.status_code == 200
    tree = html.fromstring(page.text)
    xpath = '/html/body/div[2]/div[3]/ol[1]/li/a'
    elements = tree.xpath(xpath)
    logging.info('...done')
    return elements


def _get_save_path(save_dir, anchor, regex):
    title = anchor.text
    clean_title = regex.sub('', title)
    save_path = os.path.join(save_dir, f'{clean_title}.txt')
    return save_path


def main():
    save_dir = 'downloads'
    setup_dir(save_dir)
    anchors = get_book_link_info()

    regex = re.compile(r"\(\d+\)$")
    for n, anchor in enumerate(anchors):
        logging.info(f"{n+1}/100")
        save_path = _get_save_path(save_dir, anchor, regex)
        final_url = get_final_text_url(anchor)
        if not final_url:
            continue  # no text url

        book_page = requests.get(final_url)
        assert book_page.status_code == 200, f"failed to fetch {final_url}"

        book_content = book_page.text

        with open(save_path, 'w') as f:
            f.write(book_content)


def get_final_text_url(anchor):
    href = anchor.get('href')

    book_info_url = f"https://www.gutenberg.org/{href}"
    book_info_page = requests.get(book_info_url)
    assert book_info_page.status_code == 200

    tree = html.fromstring(book_info_page.text)

    book_anchors = tree.xpath('//a')
    for o in book_anchors:
        if o.text and 'Plain Text UTF-8' in o.text:
            href = o.get('href')
            if href.startswith('//'):
                return f"https:{href}"
            return href


if __name__ == '__main__':
    main()
