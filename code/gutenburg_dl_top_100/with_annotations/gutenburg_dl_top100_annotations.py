import requests
from lxml import html
import re
import os
import logging
from typing import List,Pattern

logging.basicConfig(level=logging.INFO)


def setup_dir(save_dir:str) -> None:
    logging.info('setting up directory')
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    logging.info('...done')


def get_book_link_info() -> List[html.HtmlElement]:
    logging.info('fetching book list')
    url: str = "https://www.gutenberg.org/browse/scores/top"
    page: requests.models.Response = requests.get(url)
    assert page.status_code == 200
    tree: html.HtmlElement = html.fromstring(page.text)
    xpath: str = '/html/body/div[2]/div[3]/ol[1]/li/a'
    elements: List[html.HtmlElement] = tree.xpath(xpath)
    elements = cast(List[html.HtmlElement],elements)
    logging.info('...done')
    return elements


def _get_save_path(save_dir:str, anchor:html.HtmlElement, regex:Pattern) -> str:
    title:str = anchor.text
    clean_title:str = regex.sub('', title)
    save_path:str = os.path.join(save_dir, f'{clean_title}.txt')
    return save_path


def main():
    save_dir: str = 'downloads'
    setup_dir(save_dir)
    anchors = get_book_link_info()

    reveal_type(anchors)
    anchors: bool = get_book_link_info()

    regex: Pattern = re.compile(r"\(\d+\)$")

    for n, anchor in enumerate(anchors):
        logging.info(f"{n+1}/100")
        save_path = _get_save_path(save_dir, anchor, regex)
        final_url = get_final_text_url(anchor)
        if not final_url:
            continue  # no text url

        book_page: requests.models.Response = requests.get(final_url)
        assert book_page.status_code == 200, f"failed to fetch {final_url}"

        book_content = book_page.text

        with open(save_path, 'w') as f:
            f.write(book_content)


def get_final_text_url(anchor):
    href = anchor.get('href')

    book_info_url = f"https://www.gutenberg.org/{href}"
    book_info_page: requests.models.Response = requests.get(book_info_url)
    assert book_info_page.status_code == 200

    tree = html.fromstring(book_info_page.text)

    book_anchors = tree.xpath('//a')
    for o in book_anchors:
        if o.text and 'Plain Text UTF-8' in o.text:
            href = o.get('href')
            if href.startswith('//'):
                return f"https:{href}"
            return href


if __name__ == '__main__':
    main()
