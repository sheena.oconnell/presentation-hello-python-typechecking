class RubberDuck:
    def quack(self):
        return "squeak"

class MallardDuck:
    def quack(self):
        return "quack"

def all_the_quacks(all_the_ducks):
    for o in all_the_ducks:
        yield o.quack()

l = []
l.extend([RubberDuck() for _ in range(3)])
l.extend([MallardDuck() for _ in range(3)])
all_the_quacks(l)
