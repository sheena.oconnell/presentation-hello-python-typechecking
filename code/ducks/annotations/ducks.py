from typing import TypeVar, List, Iterator
from typing_extensions import Protocol


class SupportsQuack(Protocol):
    def quack(self)-> str:
        ...

class RubberDuck:
    def quack(self) -> str:
        return "squeak"

class MallardDuck:
    def quack(self) -> str:
        return "quack"

def all_the_quacks(all_the_ducks) -> Iterator[str]:
    for o in all_the_ducks:
        yield o.quack()

l: List[SupportsQuack] = []
l.extend([RubberDuck() for _ in range(3)])
l.extend([MallardDuck() for _ in range(3)])

all_the_quacks(l)
