class Lamp:
    on = False

    def turn_on(self):
        self.on = True

    def turn_off(self):
        self.on = False


def turn_everything_on(lamps):
    for lamp in lamps:
        lamp.turn_on()


def turn_everything_off(lamps):
    for lamp in lamps:
        lamp.turn_off()


lamps = [Lamp() for _ in range(5)]


########################################
def test_lamp_class():
    lamp = Lamp()
    assert not lamp.on # new lamps are off
    # this actually fetches the Lamp.on since lamp.on has not yet been set
    lamp.turn_on()
    assert lamp.on # new lamps are off
    lamp.turn_on()
    assert lamp.on # new lamps are off
    lamp.turn_off()
    assert not lamp.on # new lamps are off
    lamp.turn_off()
    assert not lamp.on # new lamps are off

