import random

class Foo:
    def is_valid(self):
        return random.choice([True,False])

class Bar:
    def is_valid(self):
        return random.choice([True,False])

def valid_instances(l):
    return [x for x in l if x.is_valid()]

l = []
l.extend([Foo() for _ in range(3)])
l.extend([Bar() for _ in range(3)])

valid = valid_instances(l)
