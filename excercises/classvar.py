from typing import ClassVar

class Spam:
    egg: ClassVar[int] = 5
    cheese: str = "cheddar"

inst1 = Spam()
inst2 = Spam()

# what will be printed?
# are there any type errors?

print(inst1.egg)
print(inst1.cheese)

Spam.cheese = "gouda"
inst1.cheese = "parmasan"

print(inst1.cheese)
print(inst2.cheese)
print(Spam.cheese)

Spam.egg    = "poached"
inst1.egg   = 12
Spam.cheese = 12
