from typing import Dict, Union

d1: Dict[int,str] = {1:'a',2:'b'}
d2 = {'a':1,'b':2}
d3 = {'a':1,'b':'hi there'}
d4 = {
    1 : 'a' , 2 : 222222 , 3 : 'c' , 5 : 12.3 , 6: 'f'
}
