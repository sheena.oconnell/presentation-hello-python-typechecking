class Queue:
    def __init__(self) -> None:
        self.items = []

    def push(self, item):
        self.items.append(item)

    def next(self):
        return self.items.pop(0)

    def peek(self):
        if self.items:
            return self.items[0]


int_queue = Queue()
int_queue.push(1)
int_queue.push("2") # should error

str_queue = Queue()
str_queue.push("1")
str_queue.push(2) # should error
