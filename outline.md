# 1. Intro to typing
    - available in python 2.7 and py 3, latest py 3.7 introduced new goodies?
    - ask what python versions peeps are working with and say:
            - I'll show examples in all versions
            - When we do the exercises feel free to pick whichever version is more relevant in your life
            - If you have no clear reason to go with older version then stick with the latest stuff. It's worth recognising the older stuff though




# 2. Why Type? Why not?

I assume you all have good reasons for being in this tutorial session. If you are unsure of why types are cool, or need to convince your co-workers or co-conspirators of the use of typing then this section is useful. We'll cover some common objections and their faults.

# 3. Introducing MyPy




# comment syntax versus annotations
- comments work everywhere, annotations were added later
-



<section>
                    <pre><code class="hljs" data-trim contenteditable>

                    </code></pre>
                    </section>


# 4. Py 2 Basic Examples:

pip install typing # python 2 install
mypy --py2 program.py # run mypy



# Python 2 cheatsheet

# python 3 cheatsheet

# Caesar cipher (demonstrate strings, unicode etc)

# Inverted index

# Bubble Sort (demonstrate <T>)

# common reactions to wtf situations:

## reveal_type

## type: ignore
```
# Use a "type: ignore" comment to suppress errors on a given line,
# when your code confuses mypy or runs into an outright bug in mypy.
# Good practice is to comment every "ignore" with a bug link
# (in mypy, typeshed, or your own code) or an explanation of the issue.
x = confusing_function()  # type: ignore  # https://github.com/python/mypy/issues/1167
```

## cast
```
# "cast" is a helper function that lets you override the inferred
# type of an expression. It's only for mypy -- there's no runtime check.
a = [4]
b = cast(List[int], a)  # Passes fine
c = cast(List[str], a)  # Passes fine (no runtime check)
reveal_type(c)  # -> Revealed type is 'builtins.list[builtins.str]'
print(c)  # -> [4]; the object is not cast
```


# duck-types and protocols

# Union

# Optional






# 5. Type checking during code execution?
This can be done, but not using MyPy
- MonkeyType



# promotion?
In Python 2 mode str is implicitly promoted to unicode, similar to how int is compatible with float. This is unlike bytes and str in Python 3, which are incompatible. bytes in Python 2 is equivalent to str. (This might change in the future.)


#. Introduction to Typeshed + stub files
Mypy uses a separate set of library stub files in typeshed for Python 2. Library support may vary between Python 2 and Python 3.
https://mypy.readthedocs.io/en/latest/stubs.html

https://github.com/RameshAditya/asciify/blob/master/asciify.py?
