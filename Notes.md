
 @no_type_check
 get_type_hints
typing.get_type_hints()
reveal_type
cast
__annotations__

https://python-type-checking.readthedocs.io/en/latest/types.html

484 (py 3.5) = reference standard
526 (py 3.6)
3107
mypy docs





Defensive coding

Basically you run mypy with no types to make sure it won't trip over anything, and then you slowly add types, one object at a time


https://www.python.org/dev/peps/pep-0483/

TO READ:



    PEP 3107
    PEP 484
    PEP 526
    The typing module
    mypy, the Python static type checker



mypy py >=3.4








# STEPS to add types to existing code

run
```
mypy
#then
mypy --check-untyped-defs
```
then start adding types


Projects:

Caesar cipher
Bubble sort
merge sort
inverted index


codeschool projects: GoodEats




Python2:

pip install typing
mypy --py2 program.py
comment syntax: PEP 484
syntax also valid for Python3 code

```
from typing import List

def hello(): # type: () -> None
    print 'hello'

class Example:
    def method(self, lst, opt=0, *args, **kwargs):
        # type: (List[str], int, *str, **bool) -> int
        """Docstring comes after type comment."""
        ...

```
